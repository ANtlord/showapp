#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    QTextCodec* codec = QTextCodec::codecForName("utf-8");
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);
    ui->setupUi(this);
    for(char i=1; i<=6; ++i) ui->listWidget->addItems(QStringList("Урок "+QString::number(i)));
}

Widget::~Widget()
{
    delete ui;
}
void Widget::on_listWidget_doubleClicked(const QModelIndex &index)
{
    if (ui->videoPlayer->isPlaying()){
        ui->videoPlayer->stop();
    }
    ui->videoPlayer->play(Phonon::MediaSource("Network_for_children_"+QString::number((index.row()+1))+".flv"));
}
