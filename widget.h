#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QModelIndex>
#include <QTextCodec>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    
private slots:

    void on_listWidget_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
